EMAIL-SIGNATURES FOR SCHUHMACHER EVENTSERVICE ASSOCIATES

BESCHREIBUNG
	Einheitliche E-Mail Signaturen für alle E-Mail Accounts der TLD schuhmacher.es

VORBEREITUNGEN
	Die E-Mail Programme müssen im Textmode benutzt werden (i.e. kein Richtext oder HTML). Die Signaturen sollten Standardmäßig für jede E-Mail eingeschaltet sein. Da wir den Standard-Signaturtrenner benutzen, wird beim Antworten die E-Mail Signatur automatisch ausgeschlossen.

STANDARDFORM
	--<Leerzeichen>
	Schuhmacher Eventservice
	Inszenierungen in Perfektion

	<Vorname> <Nachname>
	<Funktion im Unternehmen>
	<Titel>
	<Persönliche E-Mailadresse>
	Tel: <Persönliche Telefonnummer>
	Mobil: <Persönliche Telefonnummer>

	Standort:
	Mergenthalerstr. 4a
	60388 Frankfurt am Main
	Tel: +49 (0)69-9421430
	www.pa-sound.com

	Sitz:
	Am Sportplatz 5-7
	55595 Hüffelsheim
	Tel: +49 (0)671-92080650
	www.schuhmacher.es

COPY&PASTE FOR ALL ASSOCIATES

//DANIEL SCHUHMACHER
--<Leerzeichen>
Schuhmacher Eventservice
Inszenierungen in Perfektion
Daniel Schuhmacher, B.Eng.
Geschäftsinhaber
Meister für Veranstaltungstechnik
daniel@schuhmacher.es
Tel: +49 (0)671-92080651
Mobil: +49 (0)170-2724758

Standort:
Mergenthalerstr. 4a
60388 Frankfurt am Main
Tel: +49 (0)69-9421430
www.pa-sound.com

Sitz:
Am Sportplatz 5-7
55595 Hüffelsheim
Tel: +49 (0)671-92080650
www.schuhmacher.es

//BENJAMIN VON DER WEIDEN
--<Leerzeichen>
Schuhmacher Eventservice
Inszenierungen in Perfektion
Benjamin von der Weiden
Projektmanagement
Tontechniker (FH)
benjamin.von-der-weiden@schuhmacher.es
Tel: +49 (0)69-94214322
Mobil: +49 (0)171-9077238

Standort:
Mergenthalerstr. 4a
60388 Frankfurt am Main
Tel: +49 (0)69-9421430
www.pa-sound.com

Sitz:
Am Sportplatz 5-7
55595 Hüffelsheim
Tel: +49 (0)671-92080650
www.schuhmacher.es

//KURT BRÄUNINGER
--<Leerzeichen>
Schuhmacher Eventservice
Inszenierungen in Perfektion
Kurt Bräuninger, Dipl.-Ing. (FH)
kurt.braeuninger@schuhmacher.es
Tel: +49 (0)69-94214323
Mobil: +49 (0)160-8224465

Standort:
Mergenthalerstr. 4a
60388 Frankfurt am Main
Tel: +49 (0)69-9421430
www.pa-sound.com

Sitz:
Am Sportplatz 5-7
55595 Hüffelsheim
Tel: +49 (0)671-92080650
www.schuhmacher.es

//CRASTEN SCHNEIDER
--<Leerzeichen>
Schuhmacher Eventservice
Inszenierungen in Perfektion
Carsten Schneider
Logistik und Servicewerkstatt
Radio und Fernsehtechniker
carsten.schneider@schuhmacher.es
Tel: +49 (0)69-94214324
Mobil: +49 (0)177-3298485

Standort:
Mergenthalerstr. 4a
60388 Frankfurt am Main
Tel: +49 (0)69-9421430
www.pa-sound.com

Sitz:
Am Sportplatz 5-7
55595 Hüffelsheim
Tel: +49 (0)671-92080650
www.schuhmacher.es

//ANITA SCHUHMACHER
--<Leerzeichen>
Schuhmacher Eventservice
Inszenierungen in Perfektion
Anita Schuhmacher
Buchhaltung
anita@schuhmacher.es
Tel: +49 (0)671-92080633

Am Sportplatz 5-7
55595 Hüffelsheim
Tel: +49 (0)671-92080650
www.schuhmacher.es
